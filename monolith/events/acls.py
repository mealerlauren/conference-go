import json
import requests
from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):

    header = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": city + " " + state,
        "per_page": 1
    }
    response = requests.get("https://api.pexels.com/v1/search", params=params, headers=header)
    content = response.json()
    try:
        return { "picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather(city, state):
    lat_lon_url = f"https://api.openweathermap.org/geo/1.0/direct?q={city},US-{state},USA&appid={OPEN_WEATHER_API_KEY}"
    lat_lon_response = requests.get(lat_lon_url)

    loc_content = lat_lon_response.json()

    if not loc_content:
        return None

    try:
        lat = loc_content[0]["lat"]
        lon = loc_content[0]["lon"]
    except (KeyError, IndexError):
        return None

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(weather_url, params={"units": "imperial"})
    content = response.json()

    if not content:
        return None

    try:
        weather_description = content["weather"][0]["description"]
        temperature = content["main"]["temp"]
        return {
            "description": weather_description,
            "temp": temperature,
        }
    except (KeyError, IndexError):
        return None
